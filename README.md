# AESIR-JGI #



### What is AESIR-JGI? ###

**AESIR-JGI** (**A**utomated **E**nvironmental **S**urveillance & **I**nteractive **R**eports – **J**ane **G**oodall **I**nstitute) 
is a web map that integrates **Esri ArcGIS Online**, **DocuSign eSignature API**, and the **Conservation Standards** planning process with multiple stakeholders. 